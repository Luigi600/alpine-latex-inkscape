ARG BASE_IMAGE_VERSION=1.0.5

# container
FROM registry.gitlab.com/luigi600/alpine-latex:$BASE_IMAGE_VERSION

RUN apk add inkscape

# fallback fonts
# based on https://stackoverflow.com/a/75711621
RUN apk --no-cache add curl msttcorefonts-installer fontconfig && \
    update-ms-fonts && \
	fc-cache -f

ENTRYPOINT ["pdfconverter"]
